
Pod::Spec.new do |s|

  s.name         = "GComponentBasedLib"
  s.version      = "0.0.2"
  s.summary      = "GComponentBasedLib"
  s.description  = "GComponentBasedLib测试"
  s.homepage     = "https://gitee.com/lixuanyan/GComponentBasedLib"

#  s.license      = "MIT (example)"
 s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "lixy" => "lixy" }
  s.source       = { :git => "https://gitee.com/lixuanyan/GComponentBasedLib.git", :tag => "0.0.2" }
  s.source_files  = "Classes", "Classes/**/*.{h,m}"
  s.exclude_files = "Classes/Exclude"

end
